package com.example.marc_kacou.firstpage.elements;

import android.app.Activity;
import android.util.SparseArray;

/**
 * Created by Cong on 5/25/2015.
 */

/**
 * This class is used for getting all the datas from server
 */
public class DataControl {
    private LocalConnect localConnect;
    private RESTfulRequest rest;
    private Activity activity;


    public static SparseArray<POI> listPOIs;
    private SparseArray<Media> listMedias;
    private SparseArray<Type> listTypes;

    private OnDataReadyCallback mDataCallback;

    public DataControl(Activity activity) {
        this.activity = activity;
        localConnect = new LocalConnect(activity);
        rest = new RESTfulRequest(localConnect, this, activity);
        mDataCallback = (OnDataReadyCallback) activity;
    }

    /**
     * after verifying the version, if data needs to update,
     * RESTfulRequest will load all the data in local database.
     * And will call the function 'loadFromDB'
     */
    public void updateDB(){
        listMedias = new SparseArray<Media>();
        listTypes = new SparseArray<Type>();
        listPOIs = new SparseArray<POI>();

        rest.getRemoteVersion();
    }

    /**
     * When data is ready, it will call back to the activity.
     */
    public void loadFromDB(){
        listMedias = localConnect.getMedias();
        listTypes = localConnect.getTypes();
        listPOIs = localConnect.getPOIs(listTypes, listMedias);
        mDataCallback.onDataReadyCallback();
    }

    public interface OnDataReadyCallback {
        void onDataReadyCallback();
    }

    public SparseArray<POI> getListPOIs(){
        return listPOIs;
    }
}

package com.example.marc_kacou.firstpage;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextPaint;
import android.util.Log;
import android.util.SparseArray;

import com.example.marc_kacou.firstpage.elements.DataControl;
import com.example.marc_kacou.firstpage.elements.Media;
import com.example.marc_kacou.firstpage.elements.POI;
import com.metaio.cloud.plugin.util.MetaioCloudUtils;
import com.metaio.sdk.ARELInterpreterAndroidJava;
import com.metaio.sdk.ARViewActivity;
import com.metaio.sdk.MetaioDebug;
import com.metaio.sdk.jni.AnnotatedGeometriesGroupCallback;
import com.metaio.sdk.jni.EGEOMETRY_FOCUS_STATE;
import com.metaio.sdk.jni.IAnnotatedGeometriesGroup;
import com.metaio.sdk.jni.IGeometry;
import com.metaio.sdk.jni.IMetaioSDKCallback;
import com.metaio.sdk.jni.IRadar;
import com.metaio.sdk.jni.ImageStruct;
import com.metaio.sdk.jni.LLACoordinate;
import com.metaio.sdk.jni.Rotation;
import com.metaio.sdk.jni.SensorValues;
import com.metaio.sdk.jni.Vector3d;
import com.metaio.tools.io.AssetsManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;

/**
 * Created by Marc-Kacou on 02/05/2015.
 * Mode table d'orientation virtuelle à réalité augmentée.
 */

public class RAGeolocaliseActivity extends ARViewActivity implements LocationListener, MenuCoulissantRA.MenuCoulissantRACallbacks {

    // Sert à mettre à jour  la distance du POI qui est affichée
    private static int compteur = 0;

    // Voir MetaioSDK
    private IAnnotatedGeometriesGroup mAnnotatedGeometriesGroup;
    private MyAnnotatedGeometriesGroupCallback mAnnotatedGeometriesGroupCallback;
    private IRadar mRadar;

    private LinkedList<POIRa> mlistPOI;
    private  DetailedDialogFragment bulleInfo;


    // Sert à récupérer les coordonnées GPS de l'appareil
    private LocationManager mlocationManager;
    //  Liste des fournisseur de coordonnées GPS
    List<String> providersNames;

    public static int getIconIDFromType(String t) {

        String type = t.toUpperCase();

        if (type.equals("COMMUNE"))
            return R.drawable.commune;

        else if (type.equals("FLEUVE"))
            return R.drawable.riviere;

        else if (type.equals("RIVIERE"))
            return R.drawable.riviere;

        else if (type.equals("PORT"))
            return R.drawable.port;

        else if (type.equals("SITE CULTUREL"))
            return R.drawable.siteculturel;

        else if (type.equals("BATIMENT"))
            return R.drawable.batiment;

        else if (type.equals("CHANTIER NAVAL"))
            return R.drawable.port;

        else if (type.equals("BATEAU"))
            return R.drawable.bateau;

        else
            return R.mipmap.ic_launcher;

    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Metaio
        MetaioDebug.log(" sensor : load Configuration ");
        boolean result = metaioSDK.setTrackingConfiguration("GPS", false);
        MetaioDebug.log("Tracking data loaded: " + result);


        //GeoLocalisation
        mlocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        providersNames = mlocationManager.getAllProviders();


    }

    public void onStart() {
        super.onStart();

        try {
            AssetsManager.extractAllAssets(getApplicationContext(), BuildConfig.DEBUG);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onResume() {
        super.onResume();
        for (String name : providersNames)
            mlocationManager.requestLocationUpdates(name, 5000, 0, this);
    }

    public void onPause() {
        super.onPause();
        mlocationManager.removeUpdates(this);
    }

    public void onStop() {
        super.onStop();
    }

    protected void onDestroy() {
        // Break circular reference of Java objects
        if (mAnnotatedGeometriesGroup != null) {
            mAnnotatedGeometriesGroup.registerCallback(null);

        }

        if (mAnnotatedGeometriesGroupCallback != null) {
            mAnnotatedGeometriesGroupCallback.delete();
            mAnnotatedGeometriesGroupCallback = null;
        }

        super.onDestroy();
    }


       /*
       * Méthodes de ARViewActivity à redéfinir
       *
       * int getGUILayout() : renvoie le layout à superposer sur la video de la caméra
       * getMetaioSDKCallbackHandler() : laisser tel quel
       * loadContents() :  Appelé pour charger tout le contenu de la page
       * onGeometryTouched(IGeometry geometry ) : Si on touche un points
       * */

    @Override
    public void onBackPressed() {
        //retour au menu principal
        Intent intent = new Intent(RAGeolocaliseActivity.this, PageAccueilActivity.class);
        startActivity(intent);
        finish();
    }

    protected int getGUILayout() {
        // Retourne l'interface graphique
        return R.layout.ra_geolocalisee;
    }

    /**
     * Initialiser le contenu de la table virtuelle.
     */
    protected void loadContents() {

        mlistPOI = new LinkedList<POIRa>();
        mAnnotatedGeometriesGroup = metaioSDK.createAnnotatedGeometriesGroup();
        mAnnotatedGeometriesGroupCallback = new MyAnnotatedGeometriesGroupCallback();
        mAnnotatedGeometriesGroup.registerCallback(mAnnotatedGeometriesGroupCallback);

        //On controle le rayon de visibilité des points d'intérêt de x mm à y mm
        metaioSDK.setRendererClippingPlaneLimits(100, (float) (50 * Math.pow(10, 6)));

        loadPOI();
        addPOI();
        initRadar();


    }


    /**
     * METHODE EVENEMENTIELLES
     */

    protected IMetaioSDKCallback getMetaioSDKCallbackHandler() {
        return null;
    }

    /**
     * Cette méthode est appelée tout le temps
     * son rôle est principalement d'ajuster les propriétés géométriques
     * des poitns d'intérêt pour les rendre conforme à la réalité
     */
    public void onDrawFrame() {

        // Mise à jour des informations affichées sur le point : 200 = chaque 3 secondes
        if (compteur == 200) {

            if (mlistPOI != null && mAnnotatedGeometriesGroupCallback != null) {

                Iterator<POIRa> i = mlistPOI.iterator();
                while (i.hasNext()) {

                    POIRa poi = i.next();
                    IGeometry geo = poi.getGeometry();

                    if (!geo.getIsRendered()) {

                        mAnnotatedGeometriesGroup.removeGeometry(geo);
                        mAnnotatedGeometriesGroup.addGeometry(geo, poi);
                    }

                }

            }

            compteur = -1;
        }compteur++;


        // S'assurer que les objet 3D sur l'écran aient la bonne perspective.
        if (metaioSDK != null && mSensors != null){

                SensorValues sensorValues = mSensors.getSensorValues();

                float heading = 0.0f;
                if (sensorValues.hasAttitude())
                {
                    float m[] = new float[9];
                    sensorValues.getAttitude().getRotationMatrix(m);

                    Vector3d v = new Vector3d(m[6], m[7], m[8]);
                    v.normalize();

                    heading = (float)(-Math.atan2(v.getY(), v.getX()) - Math.PI / 2.0);
                }


                Rotation rot = new Rotation((float)(Math.PI / 2.0), 0.0f, -heading);

                if( mlistPOI != null ) {

                    Iterator<POIRa> i = mlistPOI.iterator();
                    while (i.hasNext()) {
                        POIRa poi = i.next();
                        IGeometry geo = poi.getGeometry();

                        if (geo != null) {
                            geo.setRotation(rot);
                        }
                    }

                }
        }


        super.onDrawFrame();
    }

    // Si un point d'intérêt est touché
    protected void onGeometryTouched(final IGeometry geometry) {

        MetaioDebug.log("Geometry selected: " + geometry);

        POIRa annotation = null;
        Iterator<POIRa> i = mlistPOI.iterator();
        while (i.hasNext()) {

            POIRa poi = i.next();

            IGeometry billboard = poi.getAnnotation();

            if(billboard != null)
                if (poi.getGeometry().equals(geometry) || billboard.equals(geometry)) {
                    annotation = poi;
                    break;
                }

            else
                if (poi.getGeometry().equals(geometry)) {
                    annotation = poi;
                    break;
                }

        }
        if (annotation != null) {

            final POIRa finalAnnotation = annotation;
            mSurfaceView.queueEvent(new Runnable() {

                @Override
                public void run() {

                    mRadar.setObjectsDefaultTexture(AssetsManager.getAssetPathAsFile(getApplicationContext(), "yellow.png"));
                    mRadar.setObjectTexture(finalAnnotation.getGeometry(), AssetsManager.getAssetPathAsFile(getApplicationContext(), "red.png"));
                    mAnnotatedGeometriesGroup.setSelectedGeometry(finalAnnotation.getGeometry());
                }
            });



            showBulleInfo(annotation.getPoi());
        }

    }

    /**
     * Charger  tous les points d'interêts.
     */

    private void loadPOI() {
        for (int i = 0; i < DataControl.listPOIs.size(); i++) {
            int key = DataControl.listPOIs.keyAt(i);
            POI poi = DataControl.listPOIs.get(key);
            mlistPOI.add(new POIRa(poi));
        }

    }

    /**
     * Sert à associer des " données " aux POI.
     * Ces données seront récupérées dans LoadUpdatedAnnotation pour
     * afficher les informations sur les points d'intérêts (distance, nom, ... )
     */
    private void addPOI() {

        Iterator<POIRa> i = mlistPOI.iterator();
        while (i.hasNext()) {
            POIRa poi = i.next();
            mAnnotatedGeometriesGroup.addGeometry(poi.getGeometry(), poi);
        }

    }

    /**
     * Initialiser le radar.
     * A appeler des que tous les POI ont été initialisés.
     */
    private void initRadar() {

        mRadar = metaioSDK.createRadar();
        mRadar.setBackgroundTexture(AssetsManager.getAssetPathAsFile(getApplicationContext(), "radar.png"));
        mRadar.setObjectsDefaultTexture(AssetsManager.getAssetPathAsFile(getApplicationContext(), "yellow.png"));
        mRadar.setRelativeToScreen(IGeometry.ANCHOR_TL);

        //Ajout des POIRa au radar
        Iterator<POIRa> i = mlistPOI.iterator();
        while (i.hasNext())
            mRadar.add(i.next().getGeometry());

    }

    //Bulle info décrivant en détails un POI
    private void showBulleInfo(POI poi) {

        if(bulleInfo == null )
            bulleInfo = new DetailedDialogFragment();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        bulleInfo.setPoi(poi);
        bulleInfo.show(ft, " Params");

    }

    /**
     * INTERFACE :  Location Listener
     */

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onLocationChanged(Location location) {

        if(location.getAccuracy() < 20) {
            mSensors.setManualLocation(new LLACoordinate(location.getLatitude(), location.getLongitude(), 0, 0));
            this.onGoodGPSSignal();
         }

      if(location.getAccuracy()>60)
            this.onBadGPSSignal();
    }

    /**
     * INTERFACE : MenuCoulissantRACallbacks
     */
    public void onOneItemSelected(int position) {


        Intent intent = new Intent(RAGeolocaliseActivity.this, ModePageActivity.class);
        if (position == 0) // clic sur mode carte
            intent.putExtra(PageAccueilActivity.MODE_CHOISI, 1);
        if (position == 1) // clic sur guide
            intent.putExtra(PageAccueilActivity.MODE_CHOISI, 2);
        if (position == 2) {

            new Thread(new Runnable() {
                public void run() {
                    Uri site = Uri.parse(getString(R.string.facebook));
                    Intent mIntent = new Intent(Intent.ACTION_VIEW, site);
                    startActivity(mIntent);
                }
            }).start();
        }

        if (position != 2) {
            startActivity(intent);
            finish();
        }


    }

    public Context getContext() {
        return getApplicationContext();
    }



    /*
       Classe interne
     */

    final class MyAnnotatedGeometriesGroupCallback extends AnnotatedGeometriesGroupCallback {

        ImageStruct texture;
        String[] textureHash = new String[1];
        TextPaint mPaint;
        Lock geometryLock;
        int mAnnotationBackgroundIndex;
        int[] inOutCachedAnnotationBackgroundIndex = new int[]{mAnnotationBackgroundIndex};

        Bitmap mAnnotationBackground, mEmptyStarImage, mFullStarImage;
        Bitmap[] inOutCachedBitmaps = new Bitmap[]{mAnnotationBackground, mEmptyStarImage, mFullStarImage};


        public MyAnnotatedGeometriesGroupCallback() {

            mPaint = new TextPaint();
            mPaint.setFilterBitmap(true); // enable dithering
            mPaint.setAntiAlias(true); // enable anti-aliasing
        }

        public IGeometry loadUpdatedAnnotation(IGeometry geometry, Object userData, IGeometry existingAnnotation) {
            if (geometry == null)
                return null;


            if (userData == null) {
                System.out.println(" LoadUpdateAnnotation () : userData est null ");
                return null;
            }

            POIRa annotation = ((POIRa) userData);
            float distance = (float) MetaioCloudUtils.getDistanceBetweenTwoCoordinates(geometry.getTranslationLLA(), mSensors.getLocation());
            Bitmap thumbnail = BitmapFactory.decodeResource(getResources(), getIconIDFromType(annotation.getPoi().getType().getTitre()));

            try {

                texture = ARELInterpreterAndroidJava.getAnnotationImageForPOI(annotation.getPoi().getDescription(), annotation.getPoi().getNom(), distance, null, thumbnail,
                        null,
                        metaioSDK.getRenderSize(), RAGeolocaliseActivity.this,
                        mPaint, inOutCachedBitmaps, inOutCachedAnnotationBackgroundIndex, textureHash);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (thumbnail != null) thumbnail.recycle();
                thumbnail = null;
            }

            mAnnotationBackground = inOutCachedBitmaps[0];
            mEmptyStarImage = inOutCachedBitmaps[1];
            mFullStarImage = inOutCachedBitmaps[2];
            mAnnotationBackgroundIndex = inOutCachedAnnotationBackgroundIndex[0];

            IGeometry resultGeometry = null;


            if (texture != null) {
                if (geometryLock != null)
                    geometryLock.lock();

                try {
                    resultGeometry = metaioSDK.createGeometryFromImage(textureHash[0], texture, true, false);
                } catch (Exception e) {
                    MetaioDebug.log(Log.ERROR, " LoadUpdateAnnotation () : exception avec resultGeometry");
                } finally {

                    if (geometryLock != null)
                        geometryLock.unlock();
                }
            }

            annotation.setAnnotation(resultGeometry);
            return resultGeometry;
        }


        public void onFocusStateChanged(IGeometry geometry, Object userData, EGEOMETRY_FOCUS_STATE oldState, EGEOMETRY_FOCUS_STATE newState) {
            MetaioDebug.log("Focus State");
        }

    }


    public class POIRa {

        IGeometry mGeo;
        IGeometry annotation;
        POI poi;

        public POIRa(POI poi) {


            this.poi = poi;
            LLACoordinate lla = new LLACoordinate(poi.getLatLng().latitude,poi.getLatLng().longitude, poi.getAltitude(), 0);

            final File path = AssetsManager.getAssetPathAsFile(getApplicationContext(), "ExamplePOI.obj");
            if (path != null) {
                mGeo = metaioSDK.createGeometry(path);
                mGeo.setTranslationLLA(lla);
                mGeo.setScale(4);

            } else {
                mGeo = null;
                MetaioDebug.log(Log.ERROR, "Missing files for POI geometry");
            }


        }


        public IGeometry getGeometry() {
            return mGeo;
        }
        public IGeometry getAnnotation() {
            return annotation;
        }
        public void setAnnotation(IGeometry annotation) {this.annotation = annotation;}
        public POI getPoi() { return poi; }
    }


}

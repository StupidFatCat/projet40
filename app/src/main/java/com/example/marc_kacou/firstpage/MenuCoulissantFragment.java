package com.example.marc_kacou.firstpage;

/**
 * Created by Marc-Kacou on 16/04/2015.
 */
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Menu coulissant qui permet à l'utilisateur de changer de fonctionnalités.
 * Ce menu est disponible pour les fonctionnalités " Carte ", " Guide d'utilisation "
 */
public class MenuCoulissantFragment extends Fragment {

        /**
         * Remember the position of the selected item.
         */
        private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

        /**
         * Per the design guidelines, you should show the drawer on launch until the user manually
         * expands it. This shared preference tracks this.
         */
        private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

        /**
         * A pointer to the current callbacks instance (the Activity).
         */
        private NavigationDrawerCallbacks mCallbacks;

        /**
         * Helper component that ties the action bar to the navigation drawer.
         */
        private ActionBarDrawerToggle m_actionBarIcon;

        private DrawerLayout m_layoutModePage;
        private ListView m_elementsMenu;
        private View mFragmentContainerView;

        private int mCurrentSelectedPosition = 0;
        private boolean mFromSavedInstanceState;
        private boolean mUserLearnedDrawer;

        public MenuCoulissantFragment() {
        }



        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            try {
                mCallbacks = (NavigationDrawerCallbacks) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

           //auto-generated
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
            mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

            getActionBar().setBackgroundDrawable(new ColorDrawable(Color.rgb(0x05,0x76,0xaf)));
            if (savedInstanceState != null) {
                mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
                mFromSavedInstanceState = true;
            }

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            m_elementsMenu = (ListView) inflater.inflate(R.layout.menu_coulissant, container, false);
            m_elementsMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selectItem(position);
                    //view.setBackgroundColor(Color.rgb(0,164,192));
                }


            });

            m_elementsMenu.setAdapter(new ArrayAdapter<String>(
                    getActionBar().getThemedContext(),
                    android.R.layout.simple_list_item_activated_1,
                    android.R.id.text1,
                    new String[]{
                            getString(R.string.title_section1),
                            getString(R.string.title_section2),
                            getString(R.string.title_section3),
                            getString(R.string.title_section4)
                    })
            );


            m_elementsMenu.setItemChecked(mCurrentSelectedPosition, true);



            return m_elementsMenu;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            // Indicate that this fragment would like to influence the set of actions in the action bar.
            setHasOptionsMenu(true);
        }

        @Override
        public void onDetach() {
            super.onDetach();
            mCallbacks = null;
        }


        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
        }

        @Override
        public void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);
            // Forward the new configuration the drawer toggle component.
            m_actionBarIcon.onConfigurationChanged(newConfig);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            // If the drawer is open, show the global app actions in the action bar. See also
            // showGlobalContextActionBar, which controls the top-left area of the action bar.
            if (m_layoutModePage != null && isDrawerOpen()) {

            }
            super.onCreateOptionsMenu(menu, inflater);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            if (m_actionBarIcon.onOptionsItemSelected(item)) {
                return true;
            }

            if (item.getItemId() == R.id.action_example) {
                Toast.makeText(getActivity(), "Example action.", Toast.LENGTH_SHORT).show();
                return true;
            }

            return super.onOptionsItemSelected(item);
        }


    /**
     * Savoir si le menu est ouvert ou non
     * @return true or false
     */
         public boolean isDrawerOpen() {
            return m_layoutModePage != null && m_layoutModePage.isDrawerOpen(mFragmentContainerView);
        }

        /**
         * Users of this fragment must call this method to set up the navigation drawer interactions.
         *
         * @param fragmentId   The android:id of this fragment in its activity's layout.
         * @param drawerLayout The DrawerLayout containing this fragment's UI.
         */
        public void setUp(int fragmentId, DrawerLayout drawerLayout) {
            mFragmentContainerView = getActivity().findViewById(fragmentId);
            m_layoutModePage = drawerLayout;

            // set a custom shadow that overlays the main content when the drawer opens
            m_layoutModePage.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            // set up the drawer's list view with items and click listener

            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);

            // ActionBarDrawerToggle ties together the the proper interactions
            // between the navigation drawer and the action bar app icon.
            m_actionBarIcon = new ActionBarDrawerToggle(
                    getActivity(),                    /* host Activity */
                    m_layoutModePage,                    /* DrawerLayout object */
                    R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                    R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                    R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
            ) {
                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    if (!isAdded()) {

                        return;
                    }

                    getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    if (!isAdded()) {
                        return;
                    }

                    if (!mUserLearnedDrawer) {

                        // The user manually opened the drawer; store this flag to prevent auto-showing
                        // the navigation drawer automatically in the future.
                        mUserLearnedDrawer = true;
                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                    }

                    getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
                }
            };

            // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
            // per the navigation drawer design guidelines.
            if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
                m_layoutModePage.openDrawer(mFragmentContainerView);
            }

            // Defer code dependent on restoration of previous instance state.
            m_layoutModePage.post(new Runnable() {
                @Override
                public void run() {
                    m_actionBarIcon.syncState();
                }
            });

            m_layoutModePage.setDrawerListener(m_actionBarIcon);
        }

    /**
     * Mettre manuellement à jour l'élément sélectionné du menu.
     * @param position
     *         Position à laquelle se trouve l'utilisateur.
     *         Vaut 0 pour RA, 1 pour Carte, 2 pour Guide ...
     */
        public void updateCheckedItem(int position){

             m_elementsMenu.setItemChecked(position,true);
        }

    /**
     * Permet au fragment de dire à l'activité qui l'a appelé
     * quel élément de sa liste a été sélectionné.
     *
     * @param position
     *          position de l'élément sélectionné.
     */
        private void selectItem(int position) {


                mCurrentSelectedPosition = position;

            if (m_elementsMenu != null) {

                m_elementsMenu.setItemChecked(position, true);
            }
            if (m_layoutModePage != null) {
                m_layoutModePage.closeDrawer(mFragmentContainerView);
            }
            if (mCallbacks != null) {
                mCallbacks.onNavigationDrawerItemSelected(position);
            }
        }

        private ActionBar getActionBar() {
            return ((ActionBarActivity) getActivity()).getSupportActionBar();
        }

        // Interface utilsée par le fragment pour retourner un résultat
        public static interface NavigationDrawerCallbacks {
            // Sélection d'un élément du menu coulissant
            void onNavigationDrawerItemSelected(int position);
        }
}

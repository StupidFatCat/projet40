package com.example.marc_kacou.firstpage;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.marc_kacou.firstpage.elements.Media;
import com.example.marc_kacou.firstpage.elements.POI;
import com.example.marc_kacou.firstpage.elements.Type;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Marc-Kacou on 19/04/2015.
 * C'est le mode carte de l'application mobile.
 */
public class ModeCarteFragment extends Fragment implements OnMapReadyCallback{

    // Pour zoomer directement sur la ville de Lorient
    private static final LatLng CITY_LOCATION = new LatLng(47.748228, -3.369500);

   // Pour sauvegarder la position de la caméra
    private CameraPosition mCameraPosition;

    private GoogleMap mMap = null;

    // Bulle d'information pour décrire un POI
    private DetailedDialogFragment detailedFragment;

    // Listes des points d'intérêt
    private SparseArray<POI> listPOIs = null;
    private SparseArray<Type> listTypes = null;
    private SparseArray<Media> listMedias = null;
    private boolean isPOIsAdd = false;

    public ModeCarteFragment() {     }

    public void setListPOIs(SparseArray<POI> listPOIs) {
        this.listPOIs = listPOIs;
    }

    public void setListTypes(SparseArray<Type> listTypes) {
        this.listTypes = listTypes;
    }

    public void setListMedias(SparseArray<Media> listMedias) {
        this.listMedias = listMedias;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        detailedFragment = new DetailedDialogFragment();
        // charge le contenu du fragment
        View view = inflater.inflate(R.layout.mode_carte, container, false);
//        Activity host = (Activity) view.getContext();
        SupportMapFragment mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mMapFragment != null) {
            mMapFragment.getMapAsync(this);
        } else {
            Log.i("main", "supportMapFragment is null.");
        }
        //Instancier vos composants graphique ici (faîtes vos findViewById)
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mCameraPosition != null && mMap != null) {
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
            mCameraPosition = null;
        }

    }

    @Override
    public void onPause() {

        super.onPause();

        if( mMap != null )
             mCameraPosition = mMap.getCameraPosition();

        mMap = null;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        // set info window
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                LinearLayout view = (LinearLayout) getLayoutInflater(null).inflate(R.layout.info_window, null);
                TextView name = (TextView) view.findViewById(R.id.titleMap);
                name.setText(marker.getTitle());
                return view;
            }


        });

        // set click listener of InfoWindow in order to turn into PhotoListFragment
        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            public void onInfoWindowClick(Marker marker) {
                int id = Integer.parseInt(marker.getSnippet());
                // TODO: show dialog
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                detailedFragment.setPoi(listPOIs.get(id));
                detailedFragment.show(ft, null);
            }
        });

        CameraUpdate center = CameraUpdateFactory.newLatLng(CITY_LOCATION);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        googleMap.moveCamera(center);
        googleMap.animateCamera(zoom);
        addPOIs();
    }

    // Ajouter tous les point d'intérêt
    public void addPOIs() {
        if(listPOIs != null && mMap != null && !isPOIsAdd ) {
            for (int i = 0; i < listPOIs.size(); i++) {
                int key = listPOIs.keyAt(i);
                POI poi = listPOIs.get(key);
                mMap.addMarker(new MarkerOptions().position(poi.getLatLng())
                        .title(poi.getNom())
                        .snippet(Integer.toString(poi.getIdPOI())));
                //TODO: how to know which marker depend on which poi?
            }
            isPOIsAdd = true;
        }
    }


}

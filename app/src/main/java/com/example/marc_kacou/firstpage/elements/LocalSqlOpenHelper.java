package com.example.marc_kacou.firstpage.elements;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Cong on 5/15/2015.
 */

/**
 * This class is used for creating the local database
 * when the application is installed at first time.
 */
public class LocalSqlOpenHelper extends SQLiteOpenHelper{
    public static final String DATABASE_NAME = "Loriente";
    public static final int DATABASE_VERSON = 1;


    public LocalSqlOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSON);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE IF NOT EXISTS `Pdi` (\n" +
                    "  `_id` int(11) NOT NULL,\n" +
                    "  `nom` varchar(100) NOT NULL,\n" +
                    "  `latitude` double NOT NULL,\n" +
                    "  `longitude` double NOT NULL,\n" +
                    "  `altitude` double NOT NULL,\n" +
                    "  `description` text  NOT NULL,\n" +
                    "  `affichage` tinyint(1) NOT NULL DEFAULT '1',\n" +
                    "  `date` timestamp NOT NULL, \n" +
                    "  `idType` int(11) NOT NULL, \n" +
                    "  PRIMARY KEY (`_id`) \n" +
                    ");";
            db.execSQL(sql);

            sql = "CREATE TABLE IF NOT EXISTS `Type` (\n" +
                    "  `_id` int(11) NOT NULL,\n" +
                    "  `titre` varchar(100)  NOT NULL,\n" +
                    "  `date` timestamp NOT NULL,\n" +
                    "  PRIMARY KEY (`_id`)\n" +
                    ");";
            db.execSQL(sql);

            sql = "CREATE TABLE IF NOT EXISTS `Media` (\n" +
                    "  `_id` int(11) NOT NULL,\n" +
                    "  `idPdi` int(11) NOT NULL,\n" +
                    "  `lien` varchar(400) NOT NULL,\n" +
                    "  `nature` int(11) NOT NULL,\n" +
                    "  `description` varchar(200) NOT NULL,\n" +
                    "  PRIMARY KEY (`_id`)\n" +
                    ");";
            db.execSQL(sql);

            sql = "CREATE TABLE IF NOT EXISTS `Version` (\n" +
                    "  `_id` int(11) NOT NULL,\n" +
                    "  `version` int(11) NOT NULL,\n" +
                    "  PRIMARY KEY (`_id`)\n" +
                    ");";
            db.execSQL(sql);

            sql = "INSERT INTO `Version` (_id, version) VALUES (1, 0);";
            db.execSQL(sql);
        } catch (SQLException ex){
            Log.e("Failed creating TABLES", ex.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + "'Pdi';");
            onCreate(db);
        }
    }
}

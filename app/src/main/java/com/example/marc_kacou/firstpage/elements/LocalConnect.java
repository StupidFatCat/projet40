package com.example.marc_kacou.firstpage.elements;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.SparseArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Cong on 5/17/2015.
 **/

/**
 * This class is used for reading and writing data from local database
 */
public class LocalConnect {
    private static final String TABLE_POI = "Pdi";
    private static final String TABLE_TYPE = "Type";
    private static final String TABLE_MEDIA = "Media";
    private static final String TABLE_VERSION = "Version";
    private static final String KEY_ID = "_id";
    private static final String KEY_NAME = "nom";
    private static final String KEY_LAT = "latitude";
    private static final String KEY_LNG = "longitude";
    private static final String KEY_ALT = "altitude";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_AFFICHAGE = "affichage";
    private static final String KEY_TYPE = "idType";
    private static final String KEY_DATE = "date";
    private static final String KEY_TITRE = "titre";
    private static final String KEY_LIEN = "lien";
    private static final String KEY_NATURE = "nature";
    private static final String KEY_IDPDI = "idPdi";
    private static final String KEY_VERSION = "version";
    private static final String[] COLS_MEDIA = {KEY_ID, KEY_LIEN, KEY_NATURE, KEY_IDPDI, KEY_DESCRIPTION};
    private static final String[] COLS_TYPE = {KEY_ID, KEY_TITRE, KEY_DATE};
    private static final String[] COLS_POI = {KEY_ID, KEY_NAME, KEY_LAT, KEY_LNG, KEY_ALT, KEY_DESCRIPTION, KEY_AFFICHAGE, KEY_DATE, KEY_TYPE};
    private static final String[] COLS_VERSION = {KEY_ID, KEY_VERSION};
    private SQLiteDatabase database;
    private LocalSqlOpenHelper dbHelper;

    public LocalConnect(Context context) {
        dbHelper = new LocalSqlOpenHelper(context);
    }

    //throw android.database.SQLException
    public void openWrite() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void openRead() throws SQLException {
        database = dbHelper.getReadableDatabase();
    }

    public void close() throws SQLException {
        dbHelper.close();
    }

    public boolean addPOIs(JSONArray pois) {
        try {
            openWrite();
            for (int i = 0; i < pois.length(); i++) {
                JSONObject poi = pois.getJSONObject(i);
                ContentValues values = new ContentValues();

                values.put(KEY_ID, poi.getInt("idPdi"));
                values.put(KEY_NAME, poi.getString("nom"));
                values.put(KEY_LAT, poi.getDouble("latitude"));
                values.put(KEY_LNG, poi.getDouble("longitude"));
                values.put(KEY_ALT, poi.getDouble("altitude"));
                values.put(KEY_DESCRIPTION, poi.getString("description"));
                values.put(KEY_AFFICHAGE, poi.getInt("affichage"));
                values.put(KEY_TYPE, poi.getInt("idType"));
                values.put(KEY_DATE, poi.getString("date"));
//                values.put(KEY_DATE, poi.getDate());

                database.insert(TABLE_POI, null, values);
            }
            close();
        } catch (SQLException ex) {
            Log.e("Failed in adding POI :", ex.getMessage());
            return false;
        } catch (JSONException ex) {
            Log.e("Failed JSONObject poi:", ex.getMessage());
            return false;
        } finally {
            close();
        }
        return true;
    }

    public boolean addTypes(JSONArray types) {
        try {
            openWrite();
            for (int i = 0; i < types.length(); i++) {
                ContentValues values = new ContentValues();
                JSONObject type = types.getJSONObject(i);

                values.put(KEY_ID, type.getInt("idType"));
                values.put(KEY_TITRE, type.getString("titre"));
                values.put(KEY_DATE, type.getString("date"));

                database.insert(TABLE_TYPE, null, values);

            }
            close();
        } catch (SQLException ex) {
            Log.e("Failed in adding Type :", ex.getMessage());
            return false;
        } catch (JSONException ex) {
            Log.e("Failed JSONObject type:", ex.getMessage());
            return false;
        } finally {
            close();
        }
        return true;
    }

    public boolean addMedias(JSONArray medias) {
        try {
            openWrite();
            for (int i = 0; i < medias.length(); i++) {
                ContentValues values = new ContentValues();
                JSONObject media = medias.getJSONObject(i);

                values.put(KEY_ID, media.getInt("idMedia"));
                values.put(KEY_IDPDI, media.getInt("idPdi"));
                values.put(KEY_LIEN, media.getString("lien"));
                values.put(KEY_NATURE, media.getInt("nature"));
                values.put(KEY_DESCRIPTION, media.getString("description"));

                database.insert(TABLE_MEDIA, null, values);

            }
            close();
        } catch (SQLException ex) {
            Log.e("Failed in adding Medias", ex.getMessage());
            return false;
        } catch (JSONException ex) {
            Log.e("Failed JSONObject media", ex.getMessage());
            return false;
        } finally {
            close();
        }

        return true;
    }

    public boolean updateVersion(int version) {
        try {
            openWrite();
            ContentValues values = new ContentValues();
            values.put("version", version);
            database.update(TABLE_VERSION, values, "_id = 1", null);
            return true;
        } catch (SQLException ex) {
            Log.e("Error in SQLite: ", ex.getMessage());
            return false;
        } finally {
            close();
        }
    }

    /* ------------- get medias from sqlite ---------------- */
    private Media cursorToMedia(Cursor cursor) {
        return new Media(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3), cursor.getString(4));
    }

    public SparseArray<Media> getMedias() {
        SparseArray<Media> medias = new SparseArray<Media>();
        try {
            openRead();

            Cursor cursor = database.query(TABLE_MEDIA, COLS_MEDIA, null, null, null, null, null);
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                Media media = cursorToMedia(cursor);
                medias.put(media.getId(), media);
                cursor.moveToNext();
            }
            close();
            return medias;
        } catch (SQLException ex) {
            Log.e("Error in SQLite: ", ex.getMessage());
            return medias;
        } finally {
            close();
        }
    }

    /* ------------- get types from sqlite ---------------- */
    private Type cursorToType(Cursor cursor) {
        return new Type(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
    }

    public SparseArray<Type> getTypes() {
        SparseArray<Type> types = new SparseArray<Type>();
        try {
            openRead();

            Cursor cursor = database.query(TABLE_TYPE, COLS_TYPE, null, null, null, null, null);
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                Type type = cursorToType(cursor);
                types.put(type.getId(), type);
                cursor.moveToNext();
            }
            close();
            return types;
        } catch (SQLException ex) {
            Log.e("Error in SQLite: ", ex.getMessage());
            return types;
        } finally {
            close();
        }
    }

    /* ------------- get pois from sqlite ---------------- */
    private POI cursorToPOI(Cursor cursor, SparseArray<Type> types) {
        return new POI(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3), cursor.getDouble(4), cursor.getString(5), cursor.getInt(6) == 1, cursor.getString(7), types.get(cursor.getInt(8)));
    }

    private void addMediasToPOIs(SparseArray<POI> pois, SparseArray<Media> medias) {
        for (int i = 0; i < medias.size(); i++) {
            int key = medias.keyAt(i);
            Media media = medias.get(key);
            pois.get(media.getIdPdi()).addMedia(media);
        }
    }

    public SparseArray<POI> getPOIs(SparseArray<Type> types, SparseArray<Media> medias) {
        SparseArray<POI> pois = new SparseArray<POI>();
        try {
            openRead();

            Cursor cursor = database.query(TABLE_POI, COLS_POI, null, null, null, null, null);
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                POI poi = cursorToPOI(cursor, types);
                pois.put(poi.getIdPOI(), poi);
                cursor.moveToNext();
            }
            addMediasToPOIs(pois, medias);
            close();
            return pois;
        } catch (SQLException ex) {
            Log.e("Error in SQLite: ", ex.getMessage());
            return pois;
        } finally {
            close();
        }
    }

    /* ------------- get verion from sqlite ---------------- */
    public int getVersion() {
        try {
            openRead();

            Cursor cursor = database.query(TABLE_VERSION, COLS_VERSION, "_id = 1", null, null, null, null);
            cursor.moveToFirst();

            if (!cursor.isAfterLast()) {
                return cursor.getInt(1);
            } else {
                return -1;
            }
        } catch (SQLException ex) {
            Log.e("Error in SQLite: ", ex.getMessage());
            return -1;
        } finally {
            close();
        }
    }

    /* -------------- clear data ----------------------*/
    public boolean clearData() {
        try {
            openWrite();
            database.delete(TABLE_MEDIA, null, null);
            database.delete(TABLE_POI, null, null);
            database.delete(TABLE_TYPE, null, null);

            ContentValues values = new ContentValues();
            values.put("version", 0);
            database.update(TABLE_VERSION, values, "_id = 1", null);
            return true;
        } catch (SQLException ex) {
            Log.e("Error in SQLite: ", ex.getMessage());
            return false;
        } finally {
            close();
        }
    }

}

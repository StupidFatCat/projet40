package com.example.marc_kacou.firstpage;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Marc-Kacou on 03/05/2015.
 *
 */
public class MenuCoulissantRA extends Fragment {

    // Interface avec l'activité qui contient le fragment
    private MenuCoulissantRACallbacks mCallbacks;

    // Layout qui contient le fragment
    private DrawerLayout m_layoutModePage;

    // Menu affiché
    private ListView m_elementsMenu;

    private View mFragmentContainerView;

    public MenuCoulissantRA(){}

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

            mCallbacks = (MenuCoulissantRACallbacks) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        m_elementsMenu = (ListView) inflater.inflate(R.layout.menu_coulissant, container, false);

        m_elementsMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItem(position);
             }
        });

        m_elementsMenu.setAdapter(new ArrayAdapter<String>(
                mCallbacks.getContext(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                new String[]{
                        getString(R.string.title_section2),
                        getString(R.string.title_section3),
                        getString(R.string.title_section4),
                }));


        return m_elementsMenu;

    }


    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        m_layoutModePage = drawerLayout;
        // set a custom shadow that overlays the main content when the drawer opens
        m_layoutModePage.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
    }

    //Ce qui se passe quand on appuie sur une élément de la liste
        private void selectItem(int position) {

            if (m_elementsMenu != null) {
                m_elementsMenu.setItemChecked(-1,false);
            }
            if (m_layoutModePage != null) {
                m_layoutModePage.closeDrawer(mFragmentContainerView);
            }

            if (mCallbacks != null) {
                mCallbacks.onOneItemSelected(position);
            }
        }



        //Interface pour communiquer avec l'activité qui appelle ce fragment
        public static interface MenuCoulissantRACallbacks {
            /**
             * Called when an item in the navigation drawer is selected.
             */
           public void onOneItemSelected(int position);
           public Context getContext();

        }

    }

package com.example.marc_kacou.firstpage;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.example.marc_kacou.firstpage.elements.Media;
import com.example.marc_kacou.firstpage.elements.POI;

/**
 * Created by Cong on 5/30/2015.
 * Cette classe correspond à la bulle d'information qui s'affiche lorsqu'un point d'intérêt est
 * sélectionné par l'utilisateur
  */
public class DetailedDialogFragment extends DialogFragment {

    // Point d'intérêt qu'il faut décrire
    private POI poi;
    // Webview qui se charge d'afficher la description
    private WebView webView;

    /**
     * Setter de l'attribut poi.
     * @param poi point d'intérêt à décrire
     */
    public void setPoi(POI poi) {
        this.poi = poi;
    }

    /**
     * Cette fonction permet d'ajouter un média  grâce à des fonctions
     * ecrite en javascript (addReference, addVideo, addEmbeddedVideo ... )
     *
     * @param funcName  nom de la fonction javascript
     * @param media media à ajouter
     * @return un code javascript à charger dans un Webview
     */
    private String formatFunction(String funcName, Media media){
        if (funcName == "addEmbeddedVideo")
            return String.format("javascript: %s(\'%s\', \"%s\")",funcName, media.getLien(), media.getDescription());
        return String.format("javascript: %s(\"%s\", \"%s\")",funcName, media.getLien(), media.getDescription());
    }

    /**
     * Pour charger le contenu de la page de description.
     */
    private void loadHTML() {
        SparseArray<Media> medias = poi.getMedias();
        for (int i = 0; i < medias.size(); i++) {
            int key = medias.keyAt(i);
            Media media = medias.get(key);

            switch(media.getNature()){
                case 1: // reference
                    webView.loadUrl(formatFunction("addReference", media));
                    break;
                case 2: // video
                    webView.loadUrl(formatFunction("addVideo", media));
                    break;
                case 3: // image
                    webView.loadUrl(formatFunction("addImage", media));
                    break;
                case 4: // head image
                    webView.loadUrl(formatFunction("setHeadImage", media));
                    break;
                case 5: // audio
                    break;
                case 6: // embedded video
                    webView.loadUrl(formatFunction("addEmbeddedVideo", media));
            }
        }

        webView.loadUrl(String.format("javascript: addDescription(\"%s\")", poi.getDescription()));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_custom, null);
        TextView textView = (TextView) view.findViewById(R.id.title);
        textView.setText(poi.getNom());
        webView = (WebView) view.findViewById(R.id.webview1);
        webView.loadUrl("file:///android_asset/dialog.html");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                loadHTML();
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url){

                Uri site = Uri.parse(url);
                Intent mIntent = new Intent(Intent.ACTION_VIEW, site);
                startActivity(mIntent);
                return true;
            }
        });


        Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.mipmap.ic_launcher);

        dialog.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                dismiss();

            }

        });

        return dialog;


    }
}

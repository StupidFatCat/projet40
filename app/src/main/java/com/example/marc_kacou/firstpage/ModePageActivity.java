package com.example.marc_kacou.firstpage;

/**
 * Created by Marc-Kacou on 16/04/2015.
 *
 * C'est l'activité qui contient les modes carte et guide d'utilisation de l'application.
 * Une menu coulissant est également disponible pour permettre la navifation entre
 * les différentes fonctionnalités.
 *
**/


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import com.example.marc_kacou.firstpage.elements.DataControl;


public class ModePageActivity extends ActionBarActivity
        implements MenuCoulissantFragment.NavigationDrawerCallbacks, DataControl.OnDataReadyCallback {

    private static final int RA_ACTIVITY_NUM = 0;
    private static final int MAP_FRAGMENT_NUM = 1;
    private static final int GUIDE_FRAGMENT_NUM = 2;
    private static final int FACEBOOK = 3;
    // lastFrag: last shown fragment
    // thisFrag: current fragment
    private int lastFrag = -1;
    private int thisFrag = -1;
    // flag to control the appearance of fragments
    private boolean isMapAdd = false;
    private boolean isGuideAdd = false;


    private MenuCoulissantFragment m_MenuCoulissantFragment;
    private ModeCarteFragment mCarteFrag;
    private GuideUtilisationFragment mGuideFrag;

    private int mode;
    private int lastMode;
    private DataControl dataControl;




    @Override
    public void onDataReadyCallback(){
        if (mCarteFrag == null)
            mCarteFrag = new ModeCarteFragment();
        mCarteFrag.setListPOIs(DataControl.listPOIs);
        mCarteFrag.addPOIs();
        if (mode == RA_ACTIVITY_NUM){
            putActivityInMode(mode);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mode_page);

        // getSupportFragmentManager().beginTransaction().replace(R.id.layout_page_mode, new Fragment()).commit();
        m_MenuCoulissantFragment = (MenuCoulissantFragment)
                getSupportFragmentManager().findFragmentById(R.id.frag_menu_coulissant);

        // Set up the drawer.
        m_MenuCoulissantFragment.setUp(
                R.id.frag_menu_coulissant,
                (DrawerLayout) findViewById(R.id.layout_page_mode));
        dataControl = new DataControl(this);

        getSupportActionBar().setTitle("Menu");

        // Récupérons le mode choisi par l'utilisateur
        mode = getIntent().getIntExtra(PageAccueilActivity.MODE_CHOISI, -1);
        if (mode != RA_ACTIVITY_NUM)
            putActivityInMode(mode);
        dataControl.updateDB();

    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ModePageActivity.this, PageAccueilActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void showFragment(int int_fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        lastFrag = thisFrag;
        switch (int_fragment) {
            case MAP_FRAGMENT_NUM:
                // if STATION_FRAGMENT is not the current fragment, change to it.
                if (thisFrag != MAP_FRAGMENT_NUM) {
                    if (!isMapAdd) {
                        ft.add(R.id.container, mCarteFrag);
                        isMapAdd = true;
                    }
                    // hide the last fragment
                    hideLastFragment(ft);
                    ft.show(mCarteFrag);
                    thisFrag = int_fragment;
                }
                break;
            case GUIDE_FRAGMENT_NUM:
                if (thisFrag != GUIDE_FRAGMENT_NUM) {
                    if (!isGuideAdd) {
                        ft.add(R.id.container, mGuideFrag);
                        isGuideAdd = true;
                    }
                    hideLastFragment(ft);
                    ft.show(mGuideFrag);
                    thisFrag = int_fragment;
                }
                break;

        }
//        ft.commit();
        //fix bug of support.v4
        ft.commitAllowingStateLoss();

    }

    /**
     * hide to last shown fragment in the container
     *
     * @param ft FragmentTransaction
     */
    private void hideLastFragment(FragmentTransaction ft) {
        switch (lastFrag) {
            case MAP_FRAGMENT_NUM:
                ft.hide(mCarteFrag);
                break;
            case GUIDE_FRAGMENT_NUM:
//                ft.remove(mapFragment);
//                isMapAdd = false;
                ft.hide(mGuideFrag);
                break;
        }
    }

    /**
     * Mettre l'activité dans un mode donné.
     * Soit en spécifiant le mode
     *
     * @param mode     mode dans lequel mettre l'activité dans le cas
     *                 ou on vient d'une autre activité. Cette valeur
     *                 est à mettre à "NoMode" si le cas ne se présente pas.
     */
    private void putActivityInMode(int mode) {

        switch (mode) {
            case RA_ACTIVITY_NUM:
                Intent intent = new Intent(ModePageActivity.this, RAGeolocaliseActivity.class);
                ModePageActivity.this.startActivity(intent);
                finish();
                break;
            case MAP_FRAGMENT_NUM:
                lastMode = mode;
                m_MenuCoulissantFragment.updateCheckedItem(mode);
                if (mCarteFrag == null)
                    mCarteFrag = new ModeCarteFragment();
                showFragment(mode);
                break;
            case GUIDE_FRAGMENT_NUM:
                lastMode = mode;
                m_MenuCoulissantFragment.updateCheckedItem(mode);
                if (mGuideFrag == null)
                    mGuideFrag = new GuideUtilisationFragment();
                showFragment(mode);
                break;

            case FACEBOOK:
                m_MenuCoulissantFragment.updateCheckedItem(lastMode);
                new Thread(new Runnable() {
                    public void run() {
                        Uri site = Uri.parse(getString(R.string.facebook));
                        Intent mIntent = new Intent(Intent.ACTION_VIEW, site);
                        startActivity(mIntent);
                    }
                }).start();
                break;
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        putActivityInMode(position);
    }


}

package com.example.marc_kacou.firstpage.elements;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.example.marc_kacou.firstpage.ModePageActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

/**
 * Created by Cong on 5/19/2015.
 */

/**
 * this classe is used for requiring the data from web server
 */
public class RESTfulRequest {
    private static final String url_api = "http://project.stupidfatcat.com/projet40api/v1/";

    // mobile version api key
    //md5("projet40 mobile test")
    private static final String api_key = "cc625d8fd2c17dc06a45df1b8cd607ec";
    private AsyncHttpClient client = new AsyncHttpClient();
//    private SyncHttpClient client = new SyncHttpClient();
    private int version = -1;
    private int numSuccess = 0;
    private LocalConnect localConnect;
    private DataControl dataControl;
    private Activity activity;
    private String url_medias = url_api + "medias";
    private String url_types = url_api + "types";
    private String url_pois = url_api + "pois";
    private String url_version = url_api + "version";

    public RESTfulRequest(LocalConnect localConnect, DataControl dataControl, Activity activity) {
        this.localConnect = localConnect;
        this.dataControl = dataControl;
        this.activity = activity;

    }

    public LocalConnect getLocalConnect() {
        return localConnect;
    }

    /**
     * If success, it will download data one by one
     */
    public void onGetSuccess() {
        numSuccess += 1;
        if (numSuccess == 1) {
            getTypes();
        }
        if (numSuccess == 2) {
            getPois();
        }
        if (numSuccess == 3) {
            localConnect.updateVersion(version);
            dataControl.loadFromDB();
        }
    }

    /**
     * if failure, show the info window
     * @param api_request the type of request
     */
    public void onGetFailure(String api_request) {
        if(api_request == "version"){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle("Echec de tentative de mise à jour")
                            .setMessage("Pas de connexion internet active")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //TODO : no network.
                                    if (localConnect.getVersion() > 0) {
                                        dataControl.loadFromDB();
                                    }
                                }
                            })
                            .show();

                }

            });

        }
    }

    public void getRemoteVersion() {
        RequestParams params = new RequestParams();
        params.put("apiKey", api_key);
        client.get(url_version, params, new MyJsonHandler("version", this));
    }

    public  void setVersion(int version) {
        this.version = version;
    }
    public void updateData() {
        if(localConnect.clearData()){
            getMedias();
        } else {
            //TODO: clear Data failure
        }
    }

    public void getData() {
        getMedias();
    }


    public void checkVersion(int version) {

        int localVersion = localConnect.getVersion();
        if(localVersion == -1) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle("Erreur").setMessage("Echec de vérification de la version de la base de données.")
                            .setPositiveButton("Ok", null).show();
                }
            });
            return;
        }
        if (localVersion == 0) {
            //TODO: database is empty.
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle("Votre base de données est vide")
                            .setMessage("Voulez-vous la mettre à jour?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    getData();
                                }
                            }).setNegativeButton("Non", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //TODO : no data, but canceled
                        }
                    })
                            .show();
                }
            });
        } else if (localVersion != version){
            //TODO: version is old.
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle("Votre base de données n'est pas à jour")
                            .setMessage("Voulez-vous la mettre à jour?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    updateData();
                                }
                            }).setNegativeButton("Non", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dataControl.loadFromDB();
                        }

                    })
                            .show();
                }
            });
        } else {
            dataControl.loadFromDB();
        }

    }

    public void getMedias() {
        RequestParams params = new RequestParams();
        params.put("apiKey", api_key);
        client.get(url_medias, params, new MyJsonHandler("medias", this));

    }

    public void getTypes() {
        RequestParams params = new RequestParams();
        params.put("apiKey", api_key);
        client.get(url_types, params, new MyJsonHandler("types", this));
    }

    public void getPois() {
        RequestParams params = new RequestParams();
        params.put("apiKey", api_key);
        client.get(url_pois, params, new MyJsonHandler("pois", this));
    }

    public Activity getActivity() {
        return activity;
    }
}

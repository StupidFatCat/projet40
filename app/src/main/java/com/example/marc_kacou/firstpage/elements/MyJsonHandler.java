package com.example.marc_kacou.firstpage.elements;

import android.app.ProgressDialog;
import android.util.Log;

import com.example.marc_kacou.firstpage.R;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Cong on 5/19/2015.
 */

/**
 * This class is used for making http request
 */
public class MyJsonHandler extends JsonHttpResponseHandler{
    private String api_request;
    RESTfulRequest restfulRequest;
    private ProgressDialog progressDialog;

    public MyJsonHandler(String api_request, RESTfulRequest restfulRequest) {
        this.api_request = api_request;
        this.restfulRequest = restfulRequest;
    }

    @Override
    public void onStart() {
        super.onStart();

        progressDialog = new ProgressDialog(restfulRequest.getActivity());
        if (api_request == "version")
            progressDialog.setMessage(restfulRequest.getActivity().getResources().getString(R.string.dialog_update));
        else
            progressDialog.setMessage(restfulRequest.getActivity().getResources().getString(R.string.dialog_check_version));
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    @Override
    public void onFinish() {
        super.onFinish();
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
        try {
            if (statusCode == HttpStatus.SC_OK) {
                if(api_request == "version"){
                    restfulRequest.setVersion(response.getInt("version"));
                    restfulRequest.checkVersion(response.getInt("version"));
                } else {
                    JSONArray objs = response.getJSONArray(api_request);

                    Log.d("get a objs(JSONArray):", objs.toString());
                    if(api_request == "medias")
                        restfulRequest.getLocalConnect().addMedias(objs);
                    else if(api_request == "types")
                        restfulRequest.getLocalConnect().addTypes(objs);
                    else if(api_request == "pois")
                        restfulRequest.getLocalConnect().addPOIs(objs);

                    restfulRequest.onGetSuccess();
                }
            }


        } catch (JSONException ex) {
            Log.e("get a JSON error: ", ex.getMessage());
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        super.onFailure(statusCode, headers, throwable, errorResponse);
        restfulRequest.onGetFailure(api_request);
    }
}
